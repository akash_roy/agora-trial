﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace agora_utilities
{
    public class UIElementDragger : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private bool touchActive = false;
        private PointerEventData mainPointerEventData;

        private void Awake()
        {

        }
        /*
        public override void OnDrag(PointerEventData eventData)
        {
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            base.OnDrag(eventData);
        }
        */
        public void OnBeginDrag(PointerEventData eventData)
        {
            Debug.Log("OnBeginDrag   " + eventData.position);
            
            if (mainPointerEventData == null)
            {
                mainPointerEventData = eventData;
            }
            else
            {
                mainPointerEventData = null;
            }
        }
        public void OnDrag(PointerEventData eventData)
        {
            //Debug.Log("OnDrag   " + eventData.position);

            if (mainPointerEventData.pointerId == eventData.pointerId)
            {
                Vector3 newPos = Camera.main.ScreenToWorldPoint(mainPointerEventData.position);
                transform.position = new Vector3(newPos.x, newPos.y, 0f);
            }

        }
        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("OnEndDrag   " + eventData.position);
            
            if (mainPointerEventData != null)
            {
                mainPointerEventData = null;
            }
        }

    }


}