using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
#if(UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
using UnityEngine.Android;
#endif
using System.Collections;

public class LoginController : MonoBehaviour
{
    // Use this for initialization
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
    private ArrayList permissionList = new ArrayList();
#endif
    private bool enableVideoCall = true;

    private VideoScreenController videoScreen;

    [SerializeField] private string channelName = "Hello";
    [SerializeField] private string AppID;

    void Awake()
    {
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
        permissionList.Add(Permission.Microphone);
        permissionList.Add(Permission.Camera);
#endif
        // keep this alive across scenes
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        CheckAppId();
        //JoinVideoCallChannel();
    }

    void Update()
    {
        CheckPermissions();
    }

    private void CheckAppId()
    {
        Debug.Assert(AppID.Length > 10, "Please fill in your AppId first on Game Controller object.");
        GameObject go = GameObject.Find("AppIDText");
        if (go != null)
        {
            Text appIDText = go.GetComponent<Text>();
            if (appIDText != null)
            {
                if (string.IsNullOrEmpty(AppID))
                {
                    appIDText.text = "AppID: " + "UNDEFINED!";
                }
                else
                {
                    appIDText.text = "AppID: " + AppID.Substring(0, 4) + "********" + AppID.Substring(AppID.Length - 4, 4);
                }
            }
        }
    }

    // Checks for platform dependent permissions.
    private void CheckPermissions()
    {
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
        foreach (string permission in permissionList)
        {
            if (!Permission.HasUserAuthorizedPermission(permission))
            {
                Permission.RequestUserPermission(permission);
            }
        }
#endif
    }

    private void JoinVideoCallChannel()
    {
        // create VideoScreenController if nonexistent
        if (videoScreen == null)
        {
            videoScreen = VideoScreenController.Instance; // create app
        }

        videoScreen.LoadEngine(AppID); // load engine
        
        if(channelName == null)
        {
            Debug.LogError("Specify channel name to establish video call.");
            return;
        }

        // join channel and jump to next scene
        videoScreen.JoinChannel(channelName);
        videoScreen.OnVideoCallLoaded();

        enableVideoCall = !enableVideoCall;
    }

    private void LeaveVideoCallChannel()
    {
        if (videoScreen != null)
        {
            videoScreen.LeaveChannel(); // leave channel
            videoScreen.UnloadEngine(); // delete engine
            videoScreen = null; // delete instance
        }
        //Destroy(gameObject);

        enableVideoCall = !enableVideoCall;
    }

    public void OnJoinButtonClicked()
    {
        JoinVideoCallChannel();
    }

    public void OnLeaveButtonClicked()
    {
        LeaveVideoCallChannel();
    }

    public void OnToggleVideoCallButtonClicked()
    {
        if(enableVideoCall)
        {
            JoinVideoCallChannel();
        }
        else
        {
            LeaveVideoCallChannel();
        }
    }

    public void SetChannelName(string val)
    {
        channelName = val;
    }

    private void OnApplicationPause(bool paused)
    {
        if (!ReferenceEquals(videoScreen, null))
        {
            videoScreen.EnableVideo(paused);
        }
    }

    private void OnApplicationQuit()
    {
        LeaveVideoCallChannel();
        if (!ReferenceEquals(videoScreen, null))
        {
            videoScreen.UnloadEngine();
            //videoScreen.leave(); // leave channel
            //videoScreen = null; // delete instance
        }
    }
}
