using UnityEngine;
using UnityEngine.UI;
using agora_gaming_rtc;
using agora_utilities;

// this is an example of using Agora Unity SDK
// It demonstrates:
// How to enable video
// How to join/leave channel

public class VideoScreenController: MonoBehaviour
{
    public static VideoScreenController Instance;
    
    private IRtcEngine mRtcEngine;                  // instance of agora engine

    [SerializeField] private bool useDynamicViewCreation;
    [SerializeField] private bool showOnScreenMessage;
    [SerializeField] private Text MessageText;
    
    [SerializeField] private GameObject localUserView;
    [SerializeField] private GameObject remoteUserView;
    [SerializeField] private GameObject localAvatarView;
    [SerializeField] private GameObject remoteAvatarView;

    private void Awake()
    {
        Instance = this;

        localUserView.SetActive(false);
        remoteUserView.SetActive(false);
    }

    // load agora engine
    public void LoadEngine(string appId)
    {
        // start sdk
        Debug.Log("initializeEngine");

        if (mRtcEngine != null)
        {
            Debug.Log("Engine exists. Please unload it first!");
            return;
        }

        // init engine
        mRtcEngine = IRtcEngine.GetEngine(appId);

        // enable log
        mRtcEngine.SetLogFilter(LOG_FILTER.DEBUG | LOG_FILTER.INFO | LOG_FILTER.WARNING | LOG_FILTER.ERROR | LOG_FILTER.CRITICAL);
    }

    public string GetSdkVersion()
    {
        string ver = IRtcEngine.GetSdkVersion();
        return ver;
    }

    public void JoinChannel(string channel)
    {
        Debug.Log("calling join (channel = " + channel + ")");

        if (mRtcEngine == null)
            return;

        // set callbacks (optional)
        mRtcEngine.OnJoinChannelSuccess = OnJoinChannelSuccess;
        mRtcEngine.OnUserJoined = OnUserJoined;
        mRtcEngine.OnUserOffline = OnUserOffline;
        mRtcEngine.OnWarning = (int warn, string msg) =>
        {
            Debug.LogWarningFormat("Warning code:{0} msg:{1}", warn, IRtcEngine.GetErrorDescription(warn));
        };
        mRtcEngine.OnError = HandleError;

        // enable video
        mRtcEngine.EnableVideo();
        
        // allow camera output callback
        mRtcEngine.EnableVideoObserver();

        // join channel
        mRtcEngine.JoinChannel(channel, null, 0);
    }

    public void LeaveChannel()
    {
        Debug.Log("calling leave");

        if (mRtcEngine == null)
            return;

        // leave channel
        mRtcEngine.LeaveChannel();

        // deregister video frame observers in native-c code
        mRtcEngine.DisableVideoObserver();

        // deleting local user video surface to prevent overwriting during instant join
        DestroyImmediate(localUserView.GetComponent<VideoSurface>());
        ShowLocalVideo(false);

        // deleting remote user video surface to prevent overwriting during instant join
        DestroyImmediate(remoteUserView.GetComponent<VideoSurface>());
        ShowRemoteVideo(false);
    }

    // unload agora engine
    public void UnloadEngine()
    {
        Debug.Log("calling unloadEngine");

        // delete
        if (mRtcEngine != null)
        {
            // Place this call in ApplicationQuit
            IRtcEngine.Destroy();  

            mRtcEngine = null;
        }
    }

    public void EnableVideo(bool pauseVideo)
    {
        if (mRtcEngine != null)
        {
            if (!pauseVideo)
            {
                mRtcEngine.EnableVideo();
            }
            else
            {
                mRtcEngine.DisableVideo();
            }
        }
    }

    // accessing GameObject in the Scene to set video transform delegate for statically created GameObject
    public void OnVideoCallLoaded()
    {
        if (localUserView == null)
        {
            Debug.Log("failed to find LocalUserView");
            return;
        }
        else
        {
            Debug.Log("Local User View found");

            if (localUserView.GetComponent<RawImage>() == null)
            {
                localUserView.AddComponent<RawImage>();
            }

            localUserView.AddComponent<VideoSurface>();
        }

        ShowLocalVideo(true);
    }
    
    // implement engine callbacks
    private void OnJoinChannelSuccess(string channelName, uint uid, int elapsed)
    {
        Debug.Log("JoinChannelSuccessHandler: uid = " + uid);
    }

    // When a remote user joined, this function will be called. Typically create a GameObject to render video on it.
    private void OnUserJoined(uint uid, int elapsed)
    {
        Debug.Log("onUserJoined: uid = " + uid + " elapsed = " + elapsed);

        // find a game object to render video stream from 'uid' if it already exists
        GameObject go = GameObject.Find(uid.ToString());
        if (go != null)
        {
            return; // reuse
        }

        VideoSurface videoSurface;

        if (useDynamicViewCreation)
        {
            // create a GameObject and assign to this new user
            videoSurface = MakeImageSurface(uid.ToString());
        }
        else
        {
            // use referenced object to render video feed
            videoSurface = UseExistingSurface();
        }

        if (videoSurface != null)
        {
            // configure videoSurface
            videoSurface.SetForUser(uid);
            videoSurface.SetEnable(true);
            videoSurface.SetVideoSurfaceType(AgoraVideoSurfaceType.RawImage);
            videoSurface.SetGameFps(30);
        }

        ShowRemoteVideo(true);
    }

    // When remote user is offline, this function will be called. Typically delete the GameObject for this user.
    private void OnUserOffline(uint uid, USER_OFFLINE_REASON reason)
    {
        // remove video stream
        Debug.Log("onUserOffline: uid = " + uid + " reason = " + reason);

        ShowRemoteVideo(false);

        if(useDynamicViewCreation)
        {
            // this is called in main thread
            GameObject go = GameObject.Find(uid.ToString());

            if (go != null)
            {
                Destroy(go);
            }
        }
        else
        {
            DestroyImmediate(remoteUserView.GetComponent<VideoSurface>());
        }
    }

    // Use referenced object for displaying video feed
    public VideoSurface UseExistingSurface()
    {
        VideoSurface videoSurface = remoteUserView.AddComponent<VideoSurface>();

        return videoSurface;
    }

    // Create surface to render video feed dynamically
    public VideoSurface MakeImageSurface(string goName)
    {
        GameObject go = Instantiate(remoteUserView, remoteUserView.transform);//Instantiate(remoteUserView);//new GameObject();

        if (go == null)
        {
            return null;
        }

        GameObject canvas = GameObject.Find("Canvas");

        if(canvas != null)
        {
            go.transform.SetParent(canvas.transform);
        }
        else
        {
            Debug.LogWarning("Canvas reference not available.");
        }

        go.SetActive(true);

        Quaternion targetRot = new Quaternion();
        targetRot.eulerAngles = new Vector3(0f, 0f, 180f);
        go.transform.rotation = targetRot;

        Vector3 inverseScale = new Vector3(1 / go.transform.localScale.x, 1 / go.transform.localScale.y, 1 / go.transform.localScale.z);
        go.transform.localScale = Vector3.Scale(go.transform.localScale, inverseScale);

        go.name = goName;

        RectTransform goRT = go.GetComponent<RectTransform>();
        RectTransform refRT = remoteUserView.GetComponent<RectTransform>();

        goRT.anchoredPosition = new Vector2(refRT.anchoredPosition.x, refRT.anchoredPosition.y);

        // UI Element to be renderered onto
        go.AddComponent<RawImage>();

        // return videoSurface for configuration
        VideoSurface videoSurface = go.AddComponent<VideoSurface>();
        
        return videoSurface;
    }
    
    // toggling local video and avatar 
    void ShowLocalVideo(bool arg)
    {
        localUserView.SetActive(arg);
        localAvatarView.SetActive(!arg);
    }

    // toggling remote video and avatar
    void ShowRemoteVideo(bool arg)
    {
        remoteUserView.SetActive(arg);
        remoteAvatarView.SetActive(!arg);
    }

    #region Error Handling
    private int LastError { get; set; }
    private void HandleError(int error, string msg)
    {
        if (error == LastError)
        {
            return;
        }

        msg = string.Format("Error code:{0} msg:{1}", error, IRtcEngine.GetErrorDescription(error));

        switch (error)
        {
            case 101:
                msg += "\nPlease make sure your AppId is valid and it does not require a certificate for this demo.";
                break;
        }

        Debug.LogError(msg);
        if (MessageText != null)
        {
            if (MessageText.text.Length > 0)
            {
                msg = "\n" + msg;
            }
            MessageText.text += msg;
        }

        LastError = error;
    }
    #endregion
}
